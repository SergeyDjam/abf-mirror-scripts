# ABF mirror

Scripts to create and update [https://abf.io](ABF.io) mirror (local and/or remote)  
Скрипты для создания локального и/или удаленного зеркала [https://abf.io](ABF.io)  
При написании скриптов применяется `shellcheck` из Geany (Сборка->Lint) для их проверки.

## Инструкция

Скачать эти скрипты:  
`git clone https://gitlab.com/abf-mirror/abf-mirror-scripts.git`  
(Для приема обновлений перейти в папку `abf-mirror-scripts` и сделать `git pull`)

Поставить нужные для работы пакеты (от root):

| ОС/Дистрибутив | Команда установки пакетов |
| --- | --- |
| ROSA:			| `urpmi bash lynx git`  
| Ubuntu:		| `apt install bash lynx git`  
| FreeBSD:		| `pkg install bash lynx git`  
| ALT:			| `apt-get install bash lynx git-core`  

Просто сделать или обновить зеркало платформы rosa2016.1 в домашней папке:  
`env target_dir=~/abf-mirror ./git-mirror.sh`

Поиск по локальному зеркалу:  
`grep -inHr "что ищем" ~/abf-mirror`

Пример директорий в зеркале:

```
├── ./abcm2ps
│   ├── ./abcm2ps/abcm2ps_makefile.patch
│   ├── ./abcm2ps/abcm2ps.spec
│   ├── ./abcm2ps/.abf.yml

```

## Готовые архивы

По просьбам трудящихся поставляются готовые архивы зеркала. Распаковав их, их можно обновлять этими скриптами для поддержания актуальности.  
Зайдите в раздел [Tags](https://gitlab.com/abf-mirror/abf-mirror-scripts/tags) и скачайте торрент, в котором архив в двух вариантах:

* `abf.io-import_2018.12.23_19.24.tar.xz` — тарболл
* `abf.io-import_2018.12.23_19.24.sqfs` — образ SquashFS

Монтирование SquashFS (можно также прописать в `/etc/fstab` в соответствующем формате):

```
mkdir /tmp/abf
sudo mount -o loop abf.io-import_2018.12.23_19.24.sqfs /tmp/abf
```

Теперь в папке `/tmp/abf` доступно содержимое git-репозиториев import с ABF в режиме только чтения.

`abf.io-import_2018.12.23_19.24.tar.xz` — это обычный архив, содержимое аналогично SquashFS, но требуется распаковать на диск вместо чтения из tmpfs.

Пример упаковки локального зеркала:  
`mksquashfs "/media/3TB_Toshiba_BTRFS/files/Зеркала репозиториев/abf-import/" ${HOME}/Загрузки/abf.io-import_2018.12.23_19.24.sqfs -comp xz -Xbcj ia64,x86`  
`env XZ_OPT="--extreme -9 --threads=0 -v" tar cJf ${HOME}/Загрузки/abf.io-import_2018.12.23_19.24.tar.xz .`  
([https://nixtux.ru/732](https://nixtux.ru/732))

## Скрипты

### pkg-list.sh

`pkg-list.sh` — получить список SRPM-пакетов

Управлять параметрами можно через переменные окружения:

| Переменная | Значение по умолчанию | Назначение |
| --- | --- | --- |
| mirror | http://mirror.rosalab.ru | Зеркало |
| platform | rosa2016.1 | Платформа |
| debug | no | Включить ли вывод отладочной информации. Любое значение, отличное от no, включает. |
| repos | main contrib non-free restricted | Список репозиториев, разделенный [IFS](http://fliplinux.com/ifs-n-bash.html) |
| repotypes | release testing updates | Список подрепозиториев, разделенный [IFS](http://fliplinux.com/ifs-n-bash.html) |
| projects_list_file | /tmp/abf-mirror_$(date +%s)/import_SRPM.list | Куда сохранять список SRPM-пакетов (файл) |

Если переменная не задана, то будет использоваться значение по умолчанию.

Пример запуска без доп. параметров:  
`./pkg-list.sh`

Пример запуска скрипта с установкой своих значений переменных:  
`env mirror='https://mirror.yandex.ru' platform='rosa2016.1' repos='main contrib non-free restricted' repotypes='release testing updates' projects_list_file='./file.txt' ./pkg-list.sh`

### git-mirror.sh

`git-mirror.sh` — сделать локальные копии всех git-репозиториев по именам из списка. Если копии уже есть, они обновляются.

Если не задано никаких параметров, то сначала вызывается `pkg-list.sh` (принимаются все описанные выше переменные окружения для него), затем делается `git clone -b $platform` для каждой строки списка.

| Переменная | Значение по умолчанию | Назначение |
| --- | --- | --- |
| platform | rosa2016.1 | Бранч Git |
| debug | no | Включить ли вывод отладочной информации. Любое значение, отличное от no, включает. |
| projects_list_file | - | Откуда брать список репозиториев; если не задано, то вызывается `pkg-list.sh` |
| git_uri | https://foo:bar@abf.io/import | Префикс для имени репозитория .git ( `foo:bar` необязательно заменять) |
| target_dir | /media/3TB_Toshiba_BTRFS/files/Зеркала репозиториев/abf-import/ | Директория, куда сохранять Git-репозитории |

Можно смешивать переменные для `pkg-list.sh` и `git-mirror.sh`.

## TODO

* имена некоторых git-репозиториев и SRC RPM не совпадают, пример: https://abf.io/import/webcore-fonts-restricted собирает SRPM webcore-fonts
* обслуживание нескольких бранчей (rosa2014.1, rosa2016.1, ...)
* выгрузка на Gitlab (в т.ч. выключение protected branch)
* в текущей эвристике подбора имени пакета пакеты вида ffmpeg-restricted будут пропущены и не склонированы
