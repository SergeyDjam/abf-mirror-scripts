#!/usr/bin/env bash
# Author: mikhailnov
# License: GPLv3
# Requires: bash, git

set -efu

debug="${debug:-no}"
platform="${platform:-rosa2016.1}"
[ "$debug" != 'no' ] && set -x
pkg_error_list=''
pkg_error(){
	pkg_error_list="${pkg_error_list} ${pkg}"
}

if ( set +u ; [ -z "${projects_list_file}" ] ); then
	echo "\$projects_list_file was not defined — will create it!" && . pkg-list.sh
fi
[ -n "$projects_list_file" ]
#[ -n "${target_dir+x}" ] && echo "Please set \$target_dir ­— directory, where to save git repos" && return 1

git_uri="${git_uri:-https://foo:bar@abf.io/import}"
target_dir="${target_dir:-/mnt/disk/files/Зеркала репозиториев/abf-import/}"
[ ! -d "$target_dir" ] && mkdir -p "$target_dir"

dir_exists(){
	[ ! -d "${target_dir}/${1}/.git" ] && \
		[ "$(/bin/ls -la "${target_dir}/${1}")" -lt 1 ] && \
		mv -v "${target_dir}/${1}" "${target_dir}/${1}.bak"
	pushd "${target_dir}/${1}" || return 1
	git reset --hard || :
	git pull origin || :
	popd
}

git_clone(){
	set +e ; set +u
	pkg_lwc=''; pkg_restr=''; pkg_deli1=''; pkg_deli2=''; pkg_deli1_restr=''; pkg_deli2_restr=''; pkg_nomajor=''
	
	# First, try directly and stop, if it was successfull
	# If was not, try 2 times more
	for i in {1..3}; do
		git clone -b "$1" "$2" "$3" && return 0
	done
	# If it still was unseccessfull, let's try to convert uppercase letters to lowercase, e.g.: SVGCleaner was renamed to svgcleaner
	pkg_lwc="$(echo "$pkg" | tr '[:upper:]' '[:lower:]')"
	# We may need to add '-restricted', e.g. em8300-firmware -> em8300-firmware-restricted
	pkg_restr="${pkg}-restricted"
	# let's try to remove the last part of the project name after a delimiter
	# ...and let's try to add '-restricted' after removing the last part; example: cinellera-cv -> cinellera-restricted
	echo "$pkg" | grep -q '-' && \
		pkg_deli1_tmp="$(echo "$pkg" | cut -d '-' -f1)" && \
		[ "$(echo "$pkg_deli1_tmp" | head -c 1)" != '-' ] && \
		pkg_deli1="$pkg_deli1_tmp" && pkg_deli1_restr="${pkg_deli1}-restricted"
	echo "$pkg" | grep -q '_' && \
		pkg_deli2_tmp="$(echo "$pkg" | cut -d '_' -f1)" && \
		[ "$(echo "$pkg_deli2_tmp" | head -c 1)" != '_' ] && \
		pkg_deli2="$pkg_deli2_tmp" && pkg_deli2_restr="${pkg_deli2}-restricted"
	
	# sometimes %major version is in %name, e.g. libqb0 (https://stackoverflow.com/a/10707388), libfoo0.5
	# also, lib64 can be in the name, e.g. lib64qb0
	# sed -r 's/[0-9]{1,10}$//' removes the last number, sed 's/\.$//g' removes the last dot
	echo "$pkg" | tail -c 2 | grep -qE '[0-9]' && \
		pkg_nomajor_tmp="$(echo "$pkg" | sed 's/^lib64/lib/g' | sed -r 's/[0-9]{1,10}$//' | sed 's/\.$//g')" && \
		if echo "$pkg_nomajor_tmp" | tail -c 2 | grep -qE '[0-9]'
			then pkg_nomajor="$(echo "$pkg_nomajor_tmp" | sed 's/^lib64/lib/g' | sed -r 's/[0-9]{1,10}$//' | sed 's/\.$//g')"
			else pkg_nomajor="$pkg_nomajor_tmp"
		fi
	
	pkg_new_list="$pkg_lwc $pkg_restr $pkg_deli1 $pkg_deli2 $pkg_deli1_restr $pkg_deli2_restr $pkg_nomajor"
	while read -r pkg_new
	do
		if [ ! -d "${target_dir}/${pkg_new}" ]
			then
				for i in {1..3}; do
					git clone -b "$1" "${git_uri}/${pkg_new}.git" "${target_dir}/${pkg_new}" && return 0
				done
			else ( set -eu ; dir_exists "$pkg_new" || pkg_error ) && return 0
		fi
	done < <(echo "$pkg_new_list" | tr "$IFS" "\n" | sed '/^$/d')
	
	set -e ; set -u
	# return 1 if it still was unsuccessfull
	return 1
}

while IFS='' read -r pkg
do
	if [ -d "${target_dir}/${pkg}" ]
		then
			dir_exists "$pkg"
		else
			git_clone "${platform}" "${git_uri}/${pkg}.git" "${target_dir}/${pkg}" || pkg_error
		fi
done < "${projects_list_file}"

echo "Errors list:"
echo "$pkg_error_list"
