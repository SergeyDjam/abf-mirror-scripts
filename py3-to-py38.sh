#!/usr/bin/env bash

set -eu
set +f
#dir0="${dir0:-/mnt/dev/sources/abf-mirror-scripts/}"
abf="${abf:-/mnt/dev/sources/abf-console-client-OMA/abf.py}"

#. "${dir0}/funcs.sh"

pushd "$1"
trap popd EXIT

spec="${1}.spec"
if [ ! -f "$spec" ] ; then
	spec="$(find . -type f -name '*.spec' | head -n 1)"
fi

spec2="$(echo "$spec" | sed -e 's,python-,python38-,g')"
mv -v "$spec" "$spec2" || :
git init || :
git config user.email noreply@nixtux.ru
git config user.name 'NixTux Commit Bot'
for i in spec patch diff ; do
	if ls *.${i} | grep -q '.' ; then
		git add *.${i}
	fi
done
git commit -m 'Import from Fedora 32' || :

# w/a ABF web GUI bugs in commits
sleep 1

sed -i "$spec2" \
	-e 's,__python3,__python38,g' \
	-e 's,%py3_,%py38_,g' \
	-e 's,%python3_,%python38_,g' \
	-e 's,%bcond_without tests,%bcond_with tests,g' \
	-e 's,%bcond_without python2,%bcond_with python2,g' \
	-e 's,%py38_build_wheel,%py38_build,g' \
	-e 's,python3-devel,python38-devel,g' \
	-e 's,%python3_pkgversion,38,g' \
	-e 's,%{python3_pkgversion},38,g' \
	-e 's,%{py38_prefix},python38,g' \
	-e 's,%py38_prefix,python38,g' \
	-e 's,%{python38_version_nodots},38,g' \
	-e 's,%python38_version_nodots,38,g' \
	-e 's,python3-setuptools,python38-setuptools,g' \
	-e 's,pkgconfig\(python3\),python38-devel,g' \
	-e 's,.dist-info,.egg-info,g'

sed -i "$spec2" \
	-e '/python_provide/d' \
	-e '/python3-pip/d' \
	-e '/python3-wheel/d'
sed -i "$spec2" -E -e '/^%changelog/,/(\$|^%)/{//!d}' -e '/^%changelog/d'
sed -i "$spec2" -E 	-e 's,%\{py3_,%\{py38_,g' -e 's,%\{python3_,%\{python38_,g' \
	-e 's,%\{\?dist},,g' \
	-e 's,%py38_install_wheel.*,%py38_install,g' \
	-e 's,%py38_build_wheel.*,%py38_build,g'

sed -i '1i %global __provides_exclude ^(python3egg|python3dist)' "$spec2"
sed -i '2i %global __requires_exclude ^(python3egg|python3dist)' "$spec2"

add=0 #line shifting due to newly added lines
while read -r line
do
	n=$((++line+${add}))
	sed -i "${n}i Group: Development/Python" "$spec2"
	add=$((++add))
done < <(grep -n '^Summary:' "$spec2" | awk -F ':' '{print $1}')

while read -r py3r1
do
	py3r2="$(echo "$py3r1" | sed -e 's,python3-,python38-,g' -e 's,python3dist,python3.8dist,g')"
	sed -i "$spec2" -e "s!${py3r1}!${py3r2}!g"
done < <(grep -E '^Requires:([[:blank:]])*python3' "$spec2")

while read -r py3p1
do
	py3p2="$(echo "$py3p1" | sed -e 's,python3-,python38-,g' -e 's,python3dist,python3.8dist,g')"
	sed -i "$spec2" -e "s!${py3p1}!${py3p2}!g"
done < <(grep -E '^Provides:([[:blank:]])*python3' "$spec2")

sed -i "$spec2" -e "s,-n python3-,-n python38-,g"
# fix artefact in __requires/provides_exclude
sed -i "$spec2" -e "s,(python3egg|python3.8dist),(python3egg|python3dist),g"

"$abf" put
git add *.spec .abf.yml
git commit -m 'Convert to python 3.8'

name="$(ls *.spec | head -n 1 | sed -e 's,.spec,,g')"
"$abf" create_empty --visibility public "$name" import
git remote add origin git@abf.io:import/${name}.git
git checkout -b rosa2019.05
git push --set-upstream origin rosa2019.05
git push --all
abf-add "$name" rosa2019.05
