#!/usr/bin/env bash

set -efu
dir0="${dir0:-$PWD}"
MODE="${MODE:-rpm}"

. "${dir0}/funcs.sh"

file="$1"
if [ ! -f "$file" ]; then
	echo "$file is not a file or does not exist!"
	exit 1
fi

case "$MODE" in
rpm | RPM )
	# https://docs.fedoraproject.org/en-US/packaging-guidelines/AutoProvidesAndRequiresFiltering/
	# keep this sed cmd compatible with both GNU and BSD sed
	# Also delete everything from %changelog up to end of file or any other spec section starting with '%'
	# to delete old %changelog-s imported from Mandriva or elsewhere.
	# Here we do not handle cases when a line inside %changelog starts with '%'.
	# https://nixtricks.wordpress.com/2013/01/09/sed-delete-the-lines-lying-in-between-two-patterns/
	# Delete trailing empty lines:
	# http://www.linuxask.com/questions/delete-all-trailing-blank-lines-at-end-of-file-using-sed
	# Convert *.py to *.py* because now there are .*pyc and .*pyo due to enabled byte compiling (https://stackoverflow.com/a/9591835)
	cp "$file" "$file".bak
	# Here sed -E does not work
	sed -i \
		-e "s/%global\ __requires_exclude '\(.*\)'/%global\ __requires_exclude \1/g" \
		-e "s/%global\ __requires_exclude_from '\(.*\)'/%global\ __requires_exclude_from \1/g" \
		-e "s/%global\ __provides_exclude '\(.*\)'/%global\ __provides_exclude \1/g" \
		-e "s/%global\ __provides_exclude_from '\(.*\)'/%global\ __provides_exclude_from \1/g" \
		"$file".bak
	# __provides_excludefiles did not exist, it appeared because __noautoreqfiles was incorrectly changed to __provides_excludefiles
	cat "$file".bak | sed -E \
		-e 's,^%(define|global)[[:blank:]]__noautoreqfiles,%global __requires_exclude_from,g' \
		-e 's,^%(define|global)[[:blank:]]__noautoreq,%global __requires_exclude,g' \
		-e 's,^%(define|global)[[:blank:]]__noautoprovfiles,%global __provides_exclude_from,g' \
		-e 's,^%(define|global)[[:blank:]]__noautoprov,%global __provides_exclude,g' \
		-e 's,^%(define|global)[[:blank:]]__provides_excludefiles,%global __provides_exclude_from,g' \
		-e 's,^%(define|global)[[:blank:]]__requires_excludefiles,%global __requires_exclude_from,g' \
		-e 's,^#%define[[:blank:]],#define ,g' \
		-e 's,^BuildRequires\(check\):,BuildRequires:,g' \
		-e 's,^BuildRequires\(pre\):,BuildRequires:,g' \
		-e '/^BuildRoot:/d' \
		-e 's,%configure2_5x,%configure,g' \
		-e 's,^Suggests:,Recommends:,g' \
		-e 's,^suggests:,Recommends:,g' \
		-e 's,%_rpmhome,%\{_rpmconfigdir\},g' \
		-e 's,%\{_rpmhome\},%\{_rpmconfigdir\},g' \
		-e 's,%\{_sys_macros_dir\},%\{_rpmmacrodir\},g' \
		-e 's,%_sys_macros_dir,%\{_rpmmacrodir\},g' \
		-e 's,%py_,%py2_,g' \
		-e 's,%\{py_,%\{py2_,g' \
		-e '/^%\{(py2_platsitedir|py3_platsitedir|py3_puresitedir|py_platsitedir|py_puresitedir|py_sitedir|py2_platsitedir|py2_puresitedir|py2_sitedir|python3_sitearch|python3_sitelib|python_sitearch|python_sitelib)\}.*\.py$/ s/$/\*/' \
		-e '/^%changelog/,/(\$|^%)/{//!d}' -e '/^%changelog/d' \
		| sed -e :a -e '/^\n*$/{$d;N;ba' -e '}' \
		> "$file"
# 		-e 's,%make_install,%makeinstall_std,g' \
#		-e 's,%make_build,%make,g' \
#		-e 's,%set_build_flags,%setup_compile_flags,g' \
#		-e 's,%ldflags,%build_ldflags,g' \
;;
yaml | yml | YAML | YML )
	# Remove a common artefact in .abf.yml produced by a very old version of abb
	# It worked with an old Ruby yaml parser, but does not now
	sed -i.bak \
		-e '/^---/d' \
		"$file"
;;
* )
	echo "Unknown mode!"
	exit 1
;;
esac

rm -f "${file}.bak" 2>/dev/null

pushd "$(dirname "$file")"
COMMIT_MSG="bot: rpm5 -> rpm4 (12) [rosa2019.05]

(12) rosa2019.05: fix incorrectly changed %__noautoreqfiles and %__noautoprovfiles

(11) rosa2019.05: remove quotations from %__requires_exclude and %__provides_exclude which do not work on RPM 4

(10) rosa2019.05: %py_ -> %py2_, %{py_ -> %{py2_

(9): Adopt spec for enabled byte compiling in Python

These automatic changes are done by scripts *rpm5-to-rpm4.sh at https://gitlab.com/abf-mirror/abf-mirror-scripts
The goal is to adopt specs for RPM 4 to which ROSA 2019.1 has migrated from RPM 5, but keep compatibility with RPM 5.
Details about migration are here: https://wiki.rosalab.ru/ru/index.php/Переход_ROSA_с_RPM_5_на_RPM_4
Contact m.novosyolov@rosalinux.ru in case of problems"
if ! LC_ALL=POSIX git status | grep -q 'nothing to commit'; then
	_git_commit
	_git_push
fi
popd
