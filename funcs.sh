#!/usr/bin/env bash
# common functions for sourcing 

COMMIT_AUTHOR_NAME="NixTux Commit Bot"
COMMIT_AUTHOR_EMAIL="noreply@nixtux.ru"
GIT_ADD_ALL="${GIT_ADD_ALL:-1}"

_git_commit() {
	COMMIT_MSG_FILE="$(mktemp)"
	if [ -z "$COMMIT_MSG" ]; then
		echo "Empty commit message!"
		return 1
	fi
	echo "$COMMIT_MSG" > "$COMMIT_MSG_FILE"
	# to make commit pusher correct
	git config user.name "${COMMIT_AUTHOR_NAME}"
	git config user.email "${COMMIT_AUTHOR_EMAIL}"
	if [ "$GIT_ADD_ALL" = 1 ]; then git add . ; fi
	git commit -m "$(cat "$COMMIT_MSG_FILE")" --author="${COMMIT_AUTHOR_NAME} <${COMMIT_AUTHOR_EMAIL}>"
	rm -f "$COMMIT_MSG_FILE"
}

_git_push() {
	git push
}
